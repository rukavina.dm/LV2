# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 17:59:32 2015

@author: laptop
"""
#pyton 3.4
#rukavina.dm

import urllib
from bs4 import BeautifulSoup 
import matplotlib.pyplot as plt

#http://en.wikipedia.org/wiki/Demographics_of_Croatia
url_adresa = input(str('link:'))# unos linka
html = urllib.request.urlopen(url_adresa).read() #otvaranje linka
soup = BeautifulSoup(html) # preuzimanje source koda linkna
god=[] #array za godine
data=[]#array za podatke
br="" # pomocni string niz 
table=soup.find("table", {"class":"wikitable"})#dohvacanje tablice
for row in table.findAll("tr"): # prolazak kroz redke tablice i pronalazenje tr -tablerow
    cells=row.findAll("td")#dohvacanje td.tabledata
    if(len(cells)==5):#tablica s 5 stupaca
        g=(cells[0].find(text=True))#dohvacanje prvog stupca
        d=(cells[1].find(text=True))#dohvacanje drugog stupca
        for j in range (0, len(d)):       
            if(d[j] is not ','):        
                br+=d[j] #spremanje broja od vise znamenki u string
        god.append(g) 
        data.append(float(br))
        br=""#ponistavanje broja
print(god, data)
for i in range (len(god)):
    plt.plot(god[i], data[i], 'b .')#plot petlja za svaki podatak
    