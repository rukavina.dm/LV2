# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 17:45:17 2015

@author: laptop
"""
#pyton 3.4
#rukavina.dm

import numpy as np
import matplotlib.pyplot as plt

niza=np.random.randint(1,7,100)#100 random brojeva izmedu 1i6 ukljucivo
plt.hist(niza,bins=30)#crtanje histograma
plt.xlabel('Rezultat bacanja')#dodavanje naziva osi
plt.ylabel('Broj ponavljanja')#
plt.title('Simulacija bacanja kockice')#dodavanje naslova
