# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 15:54:51 2015

@author: laptop
"""
#pyton 3.4
#rukavina.dm


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
niza=np.random.randint(1,7,30)
#plt.hist(niza,bins=30)

c=np.ones((1000,1))#array za spremanje srednjih vrijednsot
i=0
while i<1000:# ponavljanje pokusa 1000puta
    s=np.random.randint(1,7,30)#bacanje 30puta
    
    c[i]=s.mean()#dodavanje srednje vrijednost 30bacanja u niz
    i+=1
    plt.plot(c[:,0],'r .')#crtanje svakog podatka
    
#print(c.mean())    


plt.axis([0,1000,2,5])#ogranicavanje osi

plt.figure()
print('Standradna devijacija:',c.std(),'\nSrednja vrijednost:',c.mean())#ispis std i mean
df=pd.DataFrame(c)
df.plot(kind='density')#crtanje pomocu pandas dataframea
