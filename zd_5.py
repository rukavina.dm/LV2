# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 01:42:33 2015

@author: Zalman
"""
#pyton 3.4
#rukavina.dm

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

data=np.genfromtxt('F:\WinPython-64bit-3.4.3.5\python-3.4.3.amd64\Scripts\LV2\mtcars.csv',delimiter=',',dtype=str)#dohvacanje podataka iz csv dat
forow=data[1:,1] #dohvacanje svih vrijednosti prvog stupca(odnosno drugog-mpg)
secrow=data[1:,4]#dohvacanje svih vrijednosti 4 stupca(5.-hp)
sirow=data[1:,6]#dohvacanje vrijednosti tezina(wt)
#data1=pd.DataFrame(data)
#print(data[1:33,1:2])
#c=np.ones((32,1))

print('MPG',secrow)#ispis mpg
print('HP',forow)#ispis hp
plt.scatter(forow,secrow,100,'g')#prikaz podataka

plt.figure()#otvara novi prozor
fig,ax =plt.subplots()  #prikaz podataka uz odredeni podatak
ax.scatter(secrow,forow) #zadan u nizu sirow
for i,txt in enumerate(sirow):
    ax.annotate(txt,(secrow[i],forow[i]))
    
plt.show()

"""
import csv
import matplotlib.pyplot as plt
import numpy as np
#filename='mtcars.csv'
results = csv.reader(open("mtcars.csv"), delimiter=",")
re=csv.reader(open("mtcars.csv"), delimiter='\n')
#for line in results:    
data=list(results)
data2=np.array([data])
#print(next(results))
print( data2.shape ,'redaka')
#print(data2)
#print(data2[0,1,2])
#from numpy import genfromtxt
#my_data = genfromtxt('mtcars.csv', delimiter=',',dtype=float,names=True)
#for i in range(1,33):
h=data2[0,1:32,1].tolist()
g=data2[0,1:32,4].tolist()
#plt.plot(my_data)
#print((data2[0]))

dat=np.array(h)
dat1=np.array(g)
print('potrosnja',dat)
print('----------------------')
print('konjska snaga',dat1)
c=np.concatenate((h,g))
print('--------')
plt.plot(h,g)
print(data)
"""